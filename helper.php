<?php
defined('_JEXEC') or die;
abstract class ModRelationsHelper
{
    public static  function getlist(&$param) 
    {
        $typeSelected= $param->get('TypeSelector');
        if($typeSelected == 1) //lines
        {
            $itemsToList = "lines";
        }
        else if($typeSelected == 2) //programs
        {
            $itemsToList = "programs";
        }
        else if($typeSelected == 3) //centers
        {
            $itemsToList = "centers";
        }
        else if($typeSelected == 4) //domains
        {
            $itemsToList = "domains";
        }
        else
        {
            $itemsToList = "facilities";
        }
        
        $urlQuery = "";
        $returnObjName = "";
        
        $centerId = intval($param->get('centerId',0));
        $lineId = intval($param->get('lineId',0));
        $programId = intval($param->get('programId',0));
        $domainId = intval($param->get('domainId',0));
        
        if ($centerId > 0) // based on CENTER
        {
            $returnObjName = "center".$itemsToList;
            $urlQuery .= "center/center".$itemsToList."?center_id=".$centerId."&published=1";
        }
        else if ($lineId > 0)  // based on LINE
        {
            $returnObjName = "line".$itemsToList;
            $urlQuery .= "line/line".$itemsToList."?line_id=".$lineId."&published=1";
        }
        else if ($programId > 0)  // based on PROGRAM
        {
            $returnObjName = "program".$itemsToList;
            $urlQuery .= "program/program".$itemsToList."?program_id=".$programId."&published=1";
        }
        else if ($domainId > 0)  // based on PROGRAM
        {
            $returnObjName = "domain".$itemsToList;
            $urlQuery .= "domain/domain".$itemsToList."?domain_id=".$domainId."&published=1";
        }
        else //based on NOTHING - LIST 'EM ALL
        {
            $urlQuery .= $itemsToList;
            
            if($typeSelected<5)
                {
                $returnObjName = substr($itemsToList, 0, -1);
                }
            else
                {
                  $returnObjName = 'facility';  
                }
            //die($returnObjName);
            
        }
        
		$config = JFactory::getConfig();
        $api_host = $config->get('api_host');
		
        $completeUrl = 'http://'.$api_host.'/relation/'.$urlQuery;
        
        $http = new JHttp();
        $response = $http->get($completeUrl, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
	/*	
		echo '<pre>';
        print_r($completeUrl);
        echo '<br>************************<br>';
        print_r($response);
        echo '</pre>';
		*/
        if ($response->code == 200)
        {
            $tmp =  json_decode($response->body);
            return $tmp->_embedded->$returnObjName;
        }
        
        return  FALSE;
    }
    
    public static function shortenText($string, $limit, $break=".", $pad="...")
    {
      // return with no change if string is shorter than $limit
      if(strlen($string) <= $limit) return $string;

      if($break=="")
      {
          $string = substr($string, 0, $limit) . $pad;
          return $string;
      }
      
      // is $break present between $limit and the end of the string?
      if(false !== ($breakpoint = strpos($string, $break, $limit))) {
        if($breakpoint < strlen($string) - 1) {
          $string = substr($string, 0, $breakpoint) . $pad;
        }
      }

      return $string;
    }
    
    public static function getArticleFromAlias($alias)
        {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
        ->select('*')
        ->from($db->quoteName('#__content'))
        ->where($db->quoteName('alias') . ' = '. $db->quote($alias));
        $db->setQuery($query);

        $results = $db->loadAssocList();
        
        

        if(isset($results))
            return $results;
        
        return false;
        }
        
    public static function getCategoryFromId($id)
        {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
        ->select('*')
        ->from($db->quoteName('#__categories'))
        ->where($db->quoteName('id') . ' = '. $db->quote($id));
        $db->setQuery($query);

        $results = $db->loadAssocList();
        
        if(isset($results))
            return $results;
        
        return false;
        }
        
        
    public static function getMenuFromAlias($alias)
        {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
        ->select('*')
        ->from($db->quoteName('#__menu'))
        ->where($db->quoteName('alias') . ' = '. $db->quote($alias));
        $db->setQuery($query);

        $results = $db->loadAssocList();
        /*echo '<pre>';
        print_r($query->__toString());
        echo '</pre>';*/
        if(isset($results))
            return $results;
        
        return false;
        }
    
}