var timeCheck = setInterval(function () {
    checkEm();    
}, 500);

var whatToList = "";
var basedOn = "";
var apiHost = "";

function setApiHost(value)
{
    apiHost = value;
}

function checkEm()
{
    var centerIdSelectbox = document.getElementById("jform_params_centerId");
    var lineIdSelectbox = document.getElementById("jform_params_lineId");
    var programIdSelectbox = document.getElementById("jform_params_programId");
    
    var typeSelectorElm = document.getElementById("jform_params_TypeSelector");
    
    if (typeof centerIdSelectbox != "undefined" && typeof lineIdSelectbox != "undefined" && typeof programIdSelectbox != "undefined") // if are all created
    {
        
        onTypeChanged(typeSelectorElm);
        window.clearInterval(timeCheck);
    }
    
}


function onTypeChanged(t){
    
   //toggleSelectBoxes(false, false, false);
   
   if(t.value==='1')// lines
   {    
      toggleSelectBoxes(true, false, true, true);
      whatToList = "lines";
   }
   else if(t.value==='2')// programs
   {
       toggleSelectBoxes(false, true, false, false);
       whatToList = "programs";
   }
   else if(t.value==='3')// centers
   {
       toggleSelectBoxes(false, true, false, false);
       whatToList = "centers";
   }
   else if(t.value==='4')// domains
   {
       toggleSelectBoxes(false, true, false, true);
       whatToList = "domains";
   }
   else if(t.value==='5')// facilities
   {
       toggleSelectBoxes(false, false, false, false);
       whatToList = "facilities";
   }
   
   if(basedOn!="")
        getNumberOfItems(basedOn+"/"+basedOn+whatToList+"?"+basedOn+"_id="+t.value+"&published=1");
   else
        getNumberOfItems(whatToList);
}
   

function onCenterChanged(t){
   //console.log("change:"+t.value);
   basedOn = "center";
   //getNumberOfItems(basedOn+"/"+basedOn+whatToList, basedOn+"_id", t.value);
   if(whatToList!="")
        getNumberOfItems(basedOn+"/"+basedOn+whatToList+"?"+basedOn+"_id="+t.value+"&published=1");
   }
   
function onLineChanged(t){
   //console.log("change:"+t.value);
   basedOn = "line";
   if(whatToList!="")
        getNumberOfItems(basedOn+"/"+basedOn+whatToList+"?"+basedOn+"_id="+t.value+"&published=1");
   }

function onProgramChanged(t){
   //console.log("change:"+t.value);
   basedOn = "program";
   if(whatToList!="")
        getNumberOfItems(basedOn+"/"+basedOn+whatToList+"?"+basedOn+"_id="+t.value+"&published=1");
   }
   
function onDomainChanged(t){
   //console.log("change:"+t.value);
   basedOn = "domain";
   if(whatToList!="")
        getNumberOfItems(basedOn+"/"+basedOn+whatToList+"?"+basedOn+"_id="+t.value+"&published=1");
   }
   
function toggleSelectBoxes(centerActive, lineActive, programActive, domainActive)
{
    /*
    console.log("toggleSelectBoxes");
    console.log(centerActive);
    console.log(lineActive);
    console.log(programActive);
    */
//center
    if(centerActive===true){
        jQuery('#jform_params_centerId').parent().show();
    }
    else{
        jQuery('#jform_params_centerId').val(0).trigger( "liszt:updated").parent().hide();
    }
//line
    if(lineActive===true){
        jQuery('#jform_params_lineId').parent().show();
    }
    else{
        jQuery('#jform_params_lineId').val(0).trigger( "liszt:updated").parent().hide();
    }
//program
    if(programActive===true){
        jQuery('#jform_params_programId').parent().show();
    }
    else{
        jQuery('#jform_params_programId').val(0).trigger( "liszt:updated").parent().hide();
    }
//domain
    if(domainActive===true){
        jQuery('#jform_params_domainId').parent().show();
    }
    else{
        jQuery('#jform_params_domainId').val(0).trigger( "liszt:updated").parent().hide();
    }
    
    if(jQuery('#jform_params_centerId').val()==0 && jQuery('#jform_params_lineId').val()==0 && jQuery('#jform_params_programId').val()==0 && jQuery('#jform_params_domainId').val()==0)
    {
        updatelistLimitSelectbox('0');
    }

}


//function getNumberOfItems(urlAppend, param, val)
function getNumberOfItems(urlAppend)
{
    if(apiHost=="")
        return false;
    
    var url = "http://"+apiHost+"/relation/"+urlAppend;
    //console.log("URL::: "+url);
    
    jQuery.ajax({ url: url })
            .done(function(res) {
                updatelistLimitSelectbox(res.total_items);
            })
            .fail(function(e) {
                return false;
            });
}


function updatelistLimitSelectbox(limit)
{
    
    jQuery( "#jform_params_limit" ).html('').append('<option value="0">All</option>');
    
    for (i=1; i<=limit; i++){
        jQuery( "#jform_params_limit" ).append('<option value="'+i.toString()+'">'+i.toString()+'</option>');
    }
    
    jQuery( "#jform_params_limit" ).trigger( "liszt:updated");
    
}