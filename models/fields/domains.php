<?php
defined('_JEXEC') or die("domains");

jimport('joomla.form.formfield');

class FFormFieldDomains extends JFormField
{

    protected $type = 'Domains';
    
    function __construct()
    {}

    protected function getInput()
    {
        $session = JFactory::getSession();
        $options = array();
        
        $attr = '';
        $attr .= ' onchange="onDomainChanged(this);"' ;
      
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/domains';
                
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        $output = array();
        $output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->domain as $obj)
            {
                $output[$obj->id] = $obj->name_en;
            }
        }
        
        return JHtml::_('select.genericlist', $output, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        
    }
}
