<?php
defined('_JEXEC') or die("Profiles");
jimport('joomla.form.formfield');

$com_path = JPATH_SITE . '/components/com_people/';
require_once $com_path . 'models/profile.php';

class FFormFieldProfiles extends JFormField
{

    protected $type = 'Profiles';
    
    function __construct()
    {}

    protected function getInput()
    {
        $oModel = $this->getModel('profile');
		$aList = $oModel->getProfilesSimple();
		
        $options = array();
        
        $attr = '';
      
        $output = array();
        $output[0] = "Select...";
        
        if (count($aList) > 0)
        {
            foreach ($aList as $oProfile)
            {
                $output[$oProfile->id] = $oProfile->name.' ('.$oProfile->email.')';
            }
        }
        
        return JHtml::_('select.genericlist', $output, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        
    }
}
