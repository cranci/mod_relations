<?php
defined('_JEXEC') or die("centers");
jimport('joomla.form.formfield');
class FFormFieldCenters extends JFormField
{

    protected $type = 'Centers';
    
    function __construct()
    {}

    protected function getInput()
    {
        $session = JFactory::getSession();
        $options = array();
        
        $attr = '';
        $attr .= ' onchange="onCenterChanged(this);"' ;
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/centers';
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
        
          
        $output = array();
        $output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->center as $center)
            {
                $output[$center->id] = $center->name;
            }
        }
        
        return JHtml::_('select.genericlist', $output, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        
    }
}
