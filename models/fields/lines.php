<?php
defined('_JEXEC') or die("lines");
jimport('joomla.form.formfield');
class FFormFieldLines extends JFormField
{

    protected $type = 'Lines';
    
    function __construct()
    {}

    protected function getInput()
    {
        $session = JFactory::getSession();
        $options = array();
        
        $attr = '';
        $attr .= ' onchange="onLineChanged(this);"' ;
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        $api_url = 'http://'.$api_host.'/relation/lines';
        
        $http = new JHttp();
        $response = $http->get($api_url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
           
        $output = array();
        $output[0] = "Select...";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            foreach ($raw->_embedded->line as $line)
            {
                $output[$line->id] = $line->name_en;
            }
        }
        
        return JHtml::_('select.genericlist', $output, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        
    }
}
