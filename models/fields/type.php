<?php
defined('_JEXEC') or die();
jimport('joomla.form.formfield');

JHtml::script('modules/mod_relations/js/relationsConfig.js', true);

class FFormFieldType extends JFormField
{
    protected $type = 'Type';
    
    function __construct()
    {}

    protected function getInput()
    {
        
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');
        
        $session = JFactory::getSession();
        
        $attr = '';
        $attr .= ' onchange="setApiHost(\''.$api_host.'\'); onTypeChanged(this);"';
        //$attr .= ' onchange="onTypeChanged(this);"';
        $output = array();
        $output[0] = "Select...";
        $output[1] = "Research lines";
        $output[2] = "Programs";
        $output[3] = "Centers"; //not used
        $output[4] = "Domains";
        $output[5] = "Facilities";
        
        return JHtml::_('select.genericlist', $output, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        
    }
}