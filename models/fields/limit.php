<?php
defined('_JEXEC') or die("limit");
jimport('joomla.form.formfield');

class FFormFieldLimit extends JFormField
{
    protected $type = 'Limit';
    
    function __construct()
    {}

    protected function getInput()
    {
        $session = JFactory::getSession();
        
        foreach($this->form->getFieldset() as $field) {
            if ( $field->name == $this->form->getFormControl() . '[params][TypeSelector]' ) {
                // (1=line, 2=program)
                $type = $field->value;
            }
            if ( $field->name == $this->form->getFormControl() . '[params][centerId]' ) {
                if($field->value != "0")
                {
                    $listItemSelectedName = "center";
                    $listItemSelected = $field->value;
                }
            }
            if ( $field->name == $this->form->getFormControl() . '[params][lineId]' ) {
                if($field->value != "0")
                {
                    $listItemSelectedName = "line";
                    $listItemSelected = $field->value;
                }
            }
            if ( $field->name == $this->form->getFormControl() . '[params][programId]' ) {
                if($field->value != "0")
                {
                    $listItemSelectedName = "program";
                    $listItemSelected = $field->value;
                }
            }
        }
         
        
        
        if($type==1)
            $whatToList = "lines";
        else if($type==2)
            $whatToList = "programs";
         
        $config = JFactory::getConfig();
        $api_host = $config->get('api_host');    
        
        $url = "http://".$api_host."/relation/";
             
        $url .= $listItemSelectedName."/".$listItemSelectedName.$whatToList."?".$listItemSelectedName."_id=".$listItemSelected."&published=1";
         
        $http = new JHttp();
        $response = $http->get($url, array('Accept' => 'application/json','Content-Type' => 'application/json'));
          
        //echo "<pre>";
        //print_r($url);
        
        
        $output = array();
        $output[0] = "All";
        
        if ($response->code == 200)
        {
            $raw = json_decode($response->body);
            
            for ($i=1; $i<=$raw->total_items; $i++)
            {
                $output[$i] = $i;
                /*if($i==$raw->total_items)
                    $output[$i] .= " (all)";*/
            }
            
        }
        //echo "</pre>";
        
        $attr = '';
        $attr .= ' onchange=""';
        
        return JHtml::_('select.genericlist', $output, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        
    }
}