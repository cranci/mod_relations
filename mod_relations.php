<?php
defined('_JEXEC') or die();

require_once __DIR__.'/helper.php';
JHtml::stylesheet('media/mod_relations/css/site.css');

$list = ModRelationsHelper::getlist($params);
//die(var_dump($list));
$lng = (JFactory::getLanguage()->getTag() == 'it-IT') ? 'it' : 'en';
$count = $params->get('limit',4);

$mod_title_en = $params->get('list_title_en');
$mod_desc_en = $params->get('list_description_en');

$mod_title_it = $params->get('list_title_it');
$mod_desc_it = $params->get('list_description_it');

$mod_desc_limit = $params->get('desc_limit');
$bp = $params->get('breakpoint');

if($bp=="1")
    $mod_desc_breakpoint = " "; // white space      
else if($bp=="2")
    $mod_desc_breakpoint = ","; // comma
else if($bp=="3")
    $mod_desc_breakpoint = "."; // period
else
    $mod_desc_breakpoint = ""; // nothing

+
$layoutType = $params->get('layoutType');
if($layoutType==0)
    require JModuleHelper::getLayoutPath('mod_relations','noimage');
else
    require JModuleHelper::getLayoutPath('mod_relations','default');