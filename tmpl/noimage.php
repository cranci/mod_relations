<?php
defined('_JEXEC') or die();
require_once __DIR__.'/../helper.php';

$mod_title = $mod_title_en;
$mod_desc = $mod_desc_en;

if ($lng == 'it')
{
    $mod_title = $mod_title_it;
    $mod_desc = $mod_desc_it;
}
?>
<section class="iit-centre-programs">
            <h3 class="iit-title"><?php echo $mod_title; ?></h3>
            <p><?php echo $mod_desc ?></p>
            <!--
            <div class="vertical-spacer-40"></div>
            -->
                <?php 
                $c = 1;
            if ($list):
                foreach ($list as $index => $item): ?>
            <?php if ($count!=0 && $index >= $count)  {break;}?>
                <div>
                    <?php
                    if($item->state != "1"){continue;}
                    
                    $title = $item->name;
                    $alias = $item->alias;
                    //echo('<br>:::'.$alias.':::<br>');
                    $article = ModRelationsHelper::getArticleFromAlias($alias);
                    //echo('<br>:::'.$article[0]['catid'].':::<br>');
                    if(isset($article[0]))
                    {
                        $category = ModRelationsHelper::getCategoryFromId($article[0]['catid']);
                    }
                    
                    //echo(var_dump($category[0]));

                    
                    $menuItem = ModRelationsHelper::getMenuFromAlias($alias);
                    //print(var_dump($category));
                    
                    
                    if(isset($article[0]) && isset($category[0]) && isset($menuItem[0]))
                        $link = JRoute::_('index.php?option=com_content&view=article&id='.$article[0]['id'].'&catid='.$category[0]['id'].'&Itemid='.$menuItem[0]['id']);
                    else 
                        $link = "#";
                    
                    ?>
                    <strong>
                        <a class="no-image" href="<?php echo $link?>">
                        <?php echo $title?>&nbsp;&nbsp;&nbsp;<span class="fa fa-angle-right"></span>
                    </a>
                    </strong>
                </div>  
                    
            <?php 
            $c++;
            endforeach;?>
            <?php else :?>
                    <h4>No elements found.</h4>
            <?php endif;?>
</section>
          