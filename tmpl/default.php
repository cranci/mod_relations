<?php
defined('_JEXEC') or die();

require_once __DIR__.'/../helper.php';

$mod_title = $mod_title_en;
$mod_desc = $mod_desc_en;

if ($lng == 'it')
{
    $mod_title = $mod_title_it;
    $mod_desc = $mod_desc_it;
}

?>
<section class="iit-centre-programs" >
         
         <?php if(isset($mod_title)){  ?>
            <h3 class="iit-title"><?php echo $mod_title; ?></h3>
        <?php
         }
         if(isset($mod_desc)){  ?>
            <p><?php echo $mod_desc ?></p>
            <?php 
            } //end if  
			
            if(isset($mod_title) || isset($mod_desc)){
            ?>
            <div class="vertical-spacer-40"></div>
                <?php 
                } //end if
				
                $c = 1;
            if ($list):
                foreach ($list as $index => $item): ?>
            <?php if ($count!=0 && $index >= $count)  {break;}?>
            <?php 
            
			if($item->state != "1"){continue;}
		
            $title = $item->name_en;
            $description = $item->description_en;
            $alias = $item->alias_en;
                  
            if ($lng == 'it')
            {
                $title = $item->name_it;
                $description =  $item->description_it;
                
                $alias = $item->alias_it;
            }
            
            if (empty($item->img_source) && empty($description))  {continue;}?>
            
            <?php if ($c === 1) : ?>
                <div class="row">
            <?php endif;?>
                    <?php
                    
                    $article = ModRelationsHelper::getArticleFromAlias($alias);
                    if(isset($article[0]))
                    {
                        $category = ModRelationsHelper::getCategoryFromId($article[0]['catid']);
                    }
                    
                    
                    $menuItem = ModRelationsHelper::getMenuFromAlias($alias);
                  
                    if(isset($article[0]) && isset($category[0]) && isset($menuItem[0]))
                        $link = JRoute::_('index.php?option=com_content&view=article&id='.$article[0]['id'].'&catid='.$category[0]['id'].'&Itemid='.$menuItem[0]['id']);
                    else 
                        $link = "#";
                    
                    
                     $chars_limit = (int)$mod_desc_limit;
                    /*
                    if(strlen($title) <= 35){
                        $chars_limit += 50;
                    }
                    */
                    //$description = ModRelationsHelper::shortenText($description, $chars_limit, $mod_desc_breakpoint);
                    
                    ?>
                    
                    <a class="col-md-6" href="<?php echo $link?>"><!-- class="col-md-6" -->
                        <div class="iit_line_program_box">
                            <img src="<?php echo $item->img_source?>" class="iit-section-image"/>
                        </div>
                        <h4><?php echo $title?></h4>
                        <p><?php echo $description?></p>
                    </a>
                    
              <?php if ($c === 2) : ?>
                </div>
            <?php
            $c = 0;
            endif;?>      
                    
            <?php 
            $c++;
            endforeach;?>
            <?php else :?>
                    <h4>No elements found.</h4>
            <?php endif;?>
</section>
          